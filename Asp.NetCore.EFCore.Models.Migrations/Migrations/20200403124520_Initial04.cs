﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Asp.NetCore.EFCore.Models.Migrations.Migrations
{
    public partial class Initial04 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "SysUserInfo",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateTime", "LastLoginTime", "LastModifyTime" },
                values: new object[] { new DateTime(2020, 4, 3, 20, 45, 19, 958, DateTimeKind.Local).AddTicks(4963), new DateTime(2020, 4, 3, 20, 45, 19, 957, DateTimeKind.Local).AddTicks(1745), new DateTime(2020, 4, 3, 20, 45, 19, 958, DateTimeKind.Local).AddTicks(6118) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "SysUserInfo",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateTime", "LastLoginTime", "LastModifyTime" },
                values: new object[] { new DateTime(2020, 4, 3, 20, 41, 26, 710, DateTimeKind.Local).AddTicks(6977), new DateTime(2020, 4, 3, 20, 41, 26, 709, DateTimeKind.Local).AddTicks(7516), new DateTime(2020, 4, 3, 20, 41, 26, 710, DateTimeKind.Local).AddTicks(7961) });
        }
    }
}
