﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Asp.NetCore.EFCore.Models.Migrations.Migrations
{
    public partial class Initial03 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "SysUserInfo");

            migrationBuilder.DropColumn(
                name: "LastModifyId",
                table: "SysUserInfo");

            migrationBuilder.InsertData(
                table: "SysUserInfo",
                columns: new[] { "Id", "Address", "CreateId", "CreateTime", "Email", "LastLoginTime", "LastModifyTime", "Mobile", "Name", "Password", "Phone", "QQ", "Sex", "Status", "WeChat" },
                values: new object[] { 1, "朝夕教育总部", 1, new DateTime(2020, 4, 3, 20, 41, 26, 710, DateTimeKind.Local).AddTicks(6977), "18672713698@163.com", new DateTime(2020, 4, 3, 20, 41, 26, 709, DateTimeKind.Local).AddTicks(7516), new DateTime(2020, 4, 3, 20, 41, 26, 710, DateTimeKind.Local).AddTicks(7961), "18672713698", "Richard老师", "123456", "18672713698", 823626085L, (byte)1, (byte)1, "18672713698" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SysUserInfo",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "SysUserInfo",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LastModifyId",
                table: "SysUserInfo",
                type: "int",
                nullable: true);
        }
    }
}
